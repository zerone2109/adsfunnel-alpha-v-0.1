@extends('layouts.app')

@section('content')
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li>
                                <a href="#"> <a href="{{ URL::to('projects/') }}">Назад к списку</a></a>
                            </li>
                        </ol>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card-box">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h4><b>Добавление категорий страниц для сайта {{$site->Site_name}}</b></h4>
                                                <p class="text-muted m-b-30 font-13">
                                                    Для добавления/удаления используйте поле ввода ниже
                                                </p>
                                                <div class="tags-default">
                                                    <input type="text" id="category_input" value="@foreach($page_categories as $page_category){{$page_category->name}},@endforeach" data-role="tagsinput" placeholder="Добавить категорию"/>
                                                </div>

                                            </div>

                                            <div class="row">
                                                <div class="col-sm-12 form-group text-right">
                                                    <a type="button" id="save_categories" disabled=" " href="javascript:void(0);" class="btn btn-default waves-effect waves-light">
                                                        Сохранить
                                                    </a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card-box table-responsive">
                            <h4 class="m-t-0 header-title"><b>Список страниц сайта {{$site->Site_name}}</b></h4>

                            <div class="panel panel-default" >
                                <div class="panel-heading">
                                    <h4>
                                        Массовое обновление типа страниц
                                    </h4>
                                </div>
                                <div class="panel-body">
                                    {!! Form::open(array('action' => 'ProjectController@update_page', 'method' => 'post','id' => 'bulk_update')) !!}

                                    <div class="col-md-3">
                                        <input name="site" type="hidden" value="{{$site->ID}}" >
                                        {!! Form::select('condition', array('=' => 'Равно','like' => 'Содержит') , null, array('class' => 'form-control')) !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::text('update_value' , null, array('class' => 'form-control')) !!}
                                    </div>
                                    <div class="col-md-3">
                                        <select name="page_type" class="form-control">
                                            @foreach($page_types as $page_type)
                                                <option value="{{$page_type->id}}">{{$page_type->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        {!! Form::submit('Обновить', array('class' => 'form-control')) !!}
                                    </div>
                                    {!! Form::close() !!}
                                </div>

                            </div>
                            <div class="panel panel-default" >
                                <div class="panel-heading">
                                    <h4>
                                        Массовое обновление категории страниц
                                    </h4>
                                </div>
                                <div class="panel-body">
                                    {!! Form::open(array('action' => 'ProjectController@update_category_page', 'method' => 'post','id' => 'bulk_update')) !!}

                                    <div class="col-md-3">
                                        <input name="site" type="hidden" value="{{$site->ID}}" >
                                        {!! Form::select('condition', array('=' => 'Равно','like' => 'Содержит') , null, array('class' => 'form-control')) !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::text('update_value' , null, array('class' => 'form-control')) !!}
                                    </div>
                                    <div class="col-md-3">
                                        <select name="category" class="form-control">
                                            @foreach($page_categories as $page_category)
                                                <option value="{{$page_category->id}}">{{$page_category->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        {!! Form::submit('Обновить', array('class' => 'form-control')) !!}
                                    </div>
                                    {!! Form::close() !!}
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-sm-12 form-group">
                                    <a type="button" href="{{ URL::to('projects/'.$site->ID.'/add') }}" class="btn btn-default waves-effect waves-light">
                                        Добавить

                                    </a>
                                </div>
                            </div>

                            <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>URL</th>
                                    <th>Тип страницы</th>
                                    <th>Категория страницы</th>
                                    <th>Score</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($pages as $page)
                                    <tr>
                                        <td><a href="{{ URL::to('projects/'.$site->ID.'/page/'.$page->ID) }}">{{ $page->Page_url }}</a></td>
                                        <td><a href="{{ URL::to('projects/'.$site->ID.'/page/'.$page->ID) }}">{{ $page->type['name'] }}</a></td>
                                        <td><a href="{{ URL::to('projects/'.$site->ID.'/page/'.$page->ID) }}">{{ $page->category['name'] }}</a></td>
                                        <td><a href="{{ URL::to('projects/'.$site->ID.'/page/'.$page->ID) }}">{{ $page->page_score }}</a></td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div> <!-- container -->

        </div> <!-- content -->

        <footer class="footer">
            © 2017. All rights reserved.
        </footer>

    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->

@endsection
<div class="m-b-15 m-t-15">
    <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="modal"
            data-target="#custom-width-modal1">Добавить Тригер
    </button>
</div>

@if($conditions)
    @foreach ($conditions as $condition)


        <form method="POST" class="condition_form" action="javascript:void(null);">
            <div class="row m-b-10">
                <div class="col-md-8">

                    <select class="events select2 select2-multiple" multiple="multiple" multiple
                            data-placeholder="Выберите события">
                        @foreach ($events as $event)
                            <option
                                    @foreach ($condition as $condition_el)
                                        @if ($condition_el->event_id  ==  $event->id)
                                            selected
                                        @endif
                                    @endforeach
                            value="{{ $event->id }}">{{ $event->name }}
                            </option>

                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                    @foreach ($condition as $condition_el)
                        <h4 data-toggle="tooltip" data-placement="top"
                            data-html="true">{{ $condition_el->stage->name }}</h4>
                        <input type="hidden" class="stage" value="{{ $condition_el->stage->id }}">
                        @break
                    @endforeach
                </div>
                <div class="col-md-2">
                    <button id="save" type="submit"
                            class="btn btn-icon waves-effect btn-default waves-light"><i
                                class="fa fa-save"></i></button>
                    {{--<button disabled type="button" class="remove btn btn-icon waves-effect btn-warning waves-light"><i--}}
                                {{--class="fa fa-remove"></i></button>--}}
                </div>
            </div>
        </form>
    @endforeach
@endif
<div id="custom-width-modal1" class="modal fade" tabindex="-1" role="dialog"
     aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title mt-0" style="width:70%; position: absolute;">Создание
                    Webhook-a</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×
                </button>
            </div>
            <form method="POST" class="stage_condition_add" action="javascript:void(null);">
                <div class="modal-body">

                    <div class="form-group row">
                        <label class="col-2 col-form-label">Название</label>
                        <div class="col-10">
                            <select class="stages select2 "
                                    data-placeholder="Выберите этап" required>
                                @foreach ($stages as $stage)
                                    <option value="{{ $stage->id }}">{{ $stage->name }}</option>

                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Тригеры</label>
                        <div class="col-10">
                            <select class="events select2 select2-multiple" multiple="multiple" multiple
                                    data-placeholder="Выберите события" required>
                                @foreach ($events as $event)
                                    <option value="{{ $event->id }}">{{ $event->name }}</option>

                                @endforeach
                            </select>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect waves-light">Создать
                    </button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<script>
    $('.select2').select2();

    function refreshstagesdata() {
        var project_id = $('.project_select_stages').val();
        $.ajax({
            url: '{{ route('conditions_stages.get') }}',
            method: 'POST',
            data: {
                'project_id': project_id
            },
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                console.log(data);
                $('.triger-block').html(data);
                $("[data-toggle='tooltip']").tooltip();
            },
            error: function (msg) {
                console.log(msg);
            }
        });
    }

    $('.condition_form').submit(function () {

        var stage_select = $('.stage', this).val();
        var events_select = $('.events', this).val();
        var project_id = $('.project_select_stages').val();

        $.ajax({
            url: '{{ route('condition_stage.save') }}',
            method: 'POST',
            data: {
                'stage_id': stage_select,
                'project_id': project_id,
                'events_id': events_select

            },
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                console.log(data);
                $.Notification.notify('success', 'top right', 'Запись успешно сохранена');
            },
            error: function (msg) {
                console.log(msg);
                $.Notification.notify('error', 'top right', 'Ошибка при сохранении');
            }
        });

    });

    {{--$('.remove').click(function () {--}}

    {{--var webhook = $(this).closest('form').find('.webhook').val();--}}

    {{--if (confirm('Удалить вебхук?')) {--}}
    {{--$.ajax({--}}
    {{--url: '{{ route('webhook.delete') }}',--}}
    {{--method: 'POST',--}}
    {{--data: {--}}
    {{--'webhook_id': webhook--}}
    {{--},--}}
    {{--headers: {--}}
    {{--'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')--}}
    {{--},--}}
    {{--success: function (data) {--}}
    {{--console.log(data);--}}
    {{--refreshstagesdata();--}}
    {{--},--}}
    {{--error: function (msg) {--}}
    {{--console.log(msg);--}}
    {{--}--}}
    {{--});--}}
    {{--}--}}
    {{--});--}}

    $('.stage_condition_add').submit(function () {

        var stage_id = $('.stages', this).val();
        var project_id = $('.project_select_stages').val();
        var events_select = $('.events', this).val();

        $.ajax({
            url: '{{ route('conditions_stage.add') }}',
            method: 'POST',
            data: {
                'stage_id': stage_id,
                'project_id': project_id,
                'events_id': events_select
            },
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                console.log(data);
                $('#custom-width-modal1').modal('hide');
                refreshstagesdata();
            },
            error: function (msg) {
                console.log(msg);
            }
        });

    });


    $('.events').on('select2:unselecting', function (event) {
        var stage = $(this).closest('form').find('.stage').val();
        var project_id = $('.project_select_stages ').val();
        console.log(event.params.args.data.id);

        $.ajax({
            url: '{{ route('condition_stage.delete') }}',
            method: 'POST',
            data: {
                'stage_id': stage,
                'project_id': project_id,
                'event_id': event.params.args.data.id

            },
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                console.log(data);
            },
            error: function (msg) {
                console.log(msg);
            }
        });


    });
</script>
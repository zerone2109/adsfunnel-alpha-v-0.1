<!-- ========== Left Sidebar Start ========== -->

<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!--- Divider -->
        <div id="sidebar-menu">
            @if (Auth::check())
            <ul>

                {{--<li class="text-muted menu-title">More</li>--}}

                <li class="has_sub">
                    <a hhref="javascript:void(0);" class="waves-effect"><i class="ti-panel"></i><span> Тригеры </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{ URL::to('eventwebhooks/')}}"> Тригеры вебхуков</a></li>
                        <li><a href="{{ URL::to('eventstages/')}}"> Тригеры этапов</a></li>
                    </ul>
                </li>

            </ul>
            @endif
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- Left Sidebar End -->
<div class="m-b-15 m-t-15">
    <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="modal"
            data-target="#custom-width-modal1">Добавить Webhook
    </button>
</div>
@if($conditions)
    @foreach ($conditions as $condition)


        <form method="POST" class="condition_form" action="javascript:void(null);">
            <div class="row m-b-10">
                <div class="col-md-8">

                    <select class="events select2 select2-multiple" multiple="multiple" multiple
                            data-placeholder="Выберите события">
                        @foreach ($events as $event)
                            <option
                                    @foreach ($condition as $condition_el)
                                        @if ($condition_el->event_id  ==  $event->id)
                                            selected
                                        @endif
                                    @endforeach
                                    value="{{ $event->id }}">{{ $event->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                    @foreach ($condition as $condition_el)
                        <h4 data-toggle="tooltip" data-placement="top" data-html="true"
                            title="{{ $condition_el->webhook->url }}<br>{{ $condition_el->webhook->description }}">{{ $condition_el->webhook->name }}</h4>
                        <input type="hidden" class="webhook" value="{{ $condition_el->webhook_id }}">
                        @break
                    @endforeach
                </div>
                <div class="col-md-2">
                    <button id="save" type="submit"
                            class="btn btn-icon waves-effect btn-default waves-light"><i
                                class="fa fa-save"></i></button>
                    <button type="button" class="remove btn btn-icon waves-effect btn-warning waves-light"><i
                                class="fa fa-remove"></i></button>
                </div>
            </div>
        </form>
    @endforeach
@endif
<div id="custom-width-modal1" class="modal fade" tabindex="-1" role="dialog"
     aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title mt-0" style="width:70%; position: absolute;">Создание
                    Webhook-a</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×
                </button>
            </div>
            <form method="POST" class="webhook_add" action="javascript:void(null);">
                <div class="modal-body">

                    <div class="form-group row">
                        <label class="col-2 col-form-label">Название</label>
                        <div class="col-10">
                            <input type="text" name="name" class="form-control" required placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Ссылка</label>
                        <div class="col-10">
                            <input type="text" name="link" class="form-control" required placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Описание</label>
                        <div class="col-10">
                            <input type="text" name="description" class="form-control" required placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Тригеры</label>
                        <div class="col-10">
                            <select class="events select2 select2-multiple" multiple="multiple" multiple
                                    data-placeholder="Выберите события" required>
                                @foreach ($events as $event)
                                    <option value="{{ $event->id }}">{{ $event->name }}</option>

                                @endforeach
                            </select>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect waves-light">Создать
                    </button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<script>
    $('.select2').select2();

    function refreshwebhookdata() {
        var project_id = $('.project_select').val();
        $.ajax({
            url: '{{ route('conditions.get') }}',
            method: 'POST',
            data: {
                'project_id': project_id
            },
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                console.log(data);
                $('.triger-block').html(data);
                $("[data-toggle='tooltip']").tooltip();
            },
            error: function (msg) {
                console.log(msg);
            }
        });
    }

    $('.condition_form').submit(function () {

        var events_select = $('.events', this).val();
        var webhook_select = $('.webhook', this).val();
        var project_id = $('.project_select').val();

        $.ajax({
            url: '{{ route('condition.save') }}',
            method: 'POST',
            data: {
                'webhook_id': webhook_select,
                'project_id': project_id,
                'events_id': events_select

            },
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                console.log(data);
                $.Notification.notify('success', 'top right', 'Запись успешно сохранена');
            },
            error: function (msg) {
                console.log(msg);
                $.Notification.notify('error', 'top right', 'Ошибка при сохранении');
            }
        });

    });

    $('.remove').click(function () {

        var webhook = $(this).closest('form').find('.webhook').val();

        if (confirm('Удалить вебхук?')) {
            $.ajax({
                url: '{{ route('webhook.delete') }}',
                method: 'POST',
                data: {
                    'webhook_id': webhook
                },
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    console.log(data);
                    refreshwebhookdata();
                },
                error: function (msg) {
                    console.log(msg);
                }
            });
        }
    });

    $('.webhook_add').submit(function () {

        var name = $('[name="name"]', this).val();
        var link = $('[name="link"]', this).val();
        var description = $('[name="description"]', this).val();
        console.log(name + '-' + link + '-' + description);
        var project_id = $('.project_select').val();
        var events_select = $('.events', this).val();

        $.ajax({
            url: '{{ route('webhook.add') }}',
            method: 'POST',
            data: {
                'name': name,
                'link': link,
                'description': description,
                'project_id': project_id,
                'events_id': events_select

            },
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                console.log(data);
                $('#custom-width-modal1').modal('hide');
                refreshwebhookdata();
            },
            error: function (msg) {
                console.log(msg);
            }
        });

    });


    $('.events').on('select2:unselecting', function (event) {
        var webhook = $(this).closest('form').find('.webhook').val();
        var project_id = $('.project_select').val();
        console.log(event.params.args.data.id);

        $.ajax({
            url: '{{ route('condition.delete') }}',
            method: 'POST',
            data: {
                'webhook_id': webhook,
                'project_id': project_id,
                'event_id': event.params.args.data.id

            },
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                console.log(data);
            },
            error: function (msg) {
                console.log(msg);
            }
        });


    });
</script>
<!-- Top Bar Start -->
<div class="topbar">

    <!-- LOGO -->
    <div class="topbar-left">
        <div class="text-center">
            {{--<a href="index.html" class="logo"><i class="icon-magnet icon-c-logo"></i><span>Ub<i class="md md-album"></i>ld</span></a>--}}
            <!-- Image Logo here -->
           <a href="{{url('/projects')}}" class="logo">
            <i class="icon-c-logo"> <img src="{{ asset('assets/images/logo-white.png') }}" height="35"/> </i>
            <span><img src="{{ asset('assets/images/logo-white.png') }}" height="60"/></span>
           </a>
        </div>
    </div>

    <!-- Button mobile view to collapse sidebar menu -->
    <div class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="">
                <div class="pull-left">
                    <button class="button-menu-mobile open-left waves-effect waves-light">
                        <i class="md md-menu"></i>
                    </button>
                    <span class="clearfix"></span>
                </div>
                @if (Auth::check())
                <ul class="nav navbar-nav hidden-sm hidden-xs">
                    <li><a href="{{ URL::to('projects/') }}" class="waves-effect waves-light">Сайты</a></li>
                </ul>

                {{--<form role="search" class="navbar-left app-search pull-left hidden-xs">
                    <input type="text" placeholder="Поиск..." class="form-control">
                    <a href=""><i class="fa fa-search"></i></a>
                </form>--}}
                @endif

                <ul class="nav navbar-nav navbar-right pull-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        {{--<li><a href="{{ url('/register') }}">Register</a></li>--}}
                    @else
                        <li class="dropdown top-menu-item-xs">
                            <a href="#" data-target="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="true">
                                <i class="icon-bell"></i> <span class="badge badge-xs badge-danger">3</span>
                            </a>
                        </li>
                        <li class="hidden-xs">
                            <a href="#" id="btn-fullscreen" class="waves-effect waves-light"><i class="icon-size-fullscreen"></i></a>
                        </li>
                        {{--<li class="hidden-xs">
                            <a href="#" class="right-bar-toggle waves-effect waves-light"><i class="icon-settings"></i></a>
                        </li>--}}
                        <li class="dropdown top-menu-item-xs">
                            <a href="" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown" aria-expanded="true">
                                <img src="{{ asset('assets/images/users/avatar-1.jpg') }}" alt="user-img" class="img-circle">
                            </a>
                            <ul class="dropdown-menu">
                                {{--<li><a href="javascript:void(0)"><i class="ti-user m-r-10 text-custom"></i> Profile</a></li>--}}
                                <li class="divider"></li>
                                <li>
                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>

                                    <a href="{{ url('/logout') }}"
                                       onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="ti-power-off m-r-10 text-danger"></i> Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
</div>
<!-- Top Bar End -->
@extends('layouts.app')

@section('content')
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li>
                                <a href="#"> <a href="{{ URL::to('projects/'.$site->ID) }}">Назад к списку</a></a>
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <h4 class="m-t-0 header-title"><b>Редактирование страницы</b></h4>



                                    {!! Form::open(array('action' => 'ProjectController@update_page', 'method' => 'post','id' => 'bulk_update')) !!}

                                    <div class="row">
                                        <div class="col-md-5 form-group">
                                            <input name="page" type="hidden" value="{{$page->ID}}" >
                                            <input name="site" type="hidden" value="{{$site->ID}}" >

                                            {!! Form::text('update_value' , $page->Page_url, array('class' => 'form-control')) !!}
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-5 form-group">
                                            <select name="page_type" class="form-control">
                                                @foreach($page_types as $page_type)
                                                    <option @if($page_type->id == $page->page_type_id) selected @endif value="{{$page_type->id}}">{{$page_type->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5 form-group">
                                            {!! Form::text('score' ,$page->page_score, array('class' => 'form-control','placeholder' => 'score','required')) !!}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5 form-group">
                                            <select name="category" class="form-control">
                                                @foreach($page_categories as $page_category)
                                                    <option @if($page_category->id == $page->category_id) selected @endif value="{{$page_category->id}}">{{$page_category->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                            @if($page->page_type_id == 2)
                                <div class="row">
                                    <div class="col-md-5 form-group">
                                        {!! Form::text('title' ,$page->title, array('class' => 'form-control','placeholder' => 'title','required')) !!}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-5 form-group">
                                        {!! Form::number('price' ,$page->price, array('class' => 'form-control','step' => '0.1','placeholder' => 'price','required')) !!}
                                    </div>
                                </div>
                            @endif
                                    <div class="row">
                                        <div class="col-md-2 form-group">
                                            {!! Form::submit('Обновить', array('class' => 'form-control waves')) !!}
                                        </div>
                                    </div>

                                    {!! Form::close() !!}


</div>
</div>
</div>

</div> <!-- container -->

</div> <!-- content -->

<footer class="footer">
© 2017. All rights reserved.
</footer>

</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->





@endsection
@extends('layouts.app')

@section('content')
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li>
                                <a href="#"> <a href="{{ URL::to('projects/'.$site->ID) }}">Назад к списку</a></a>
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <h4 class="m-t-0 header-title"><b>Добавление страницы</b></h4>

                                {!! Form::open(array('action' => 'ProjectController@create_page', 'method' => 'post','id' => 'bulk_update')) !!}

                                <div class="row">
                                    <div class="col-md-5 form-group">
                                        <input name="site" type="hidden" value="{{$site->ID}}" >
                                        <input name="add_new" type="hidden" value="new" >
                                        {!! Form::text('url_value' ,null, array('class' => 'form-control','placeholder' => 'url','required')) !!}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-5 form-group">
                                        <select name="page_type" class="form-control">
                                            @foreach($page_types as $page_type)
                                                <option  value="{{$page_type->id}}">{{$page_type->name}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-5 form-group">
                                        {!! Form::text('score' ,null, array('class' => 'form-control','placeholder' => 'score','required')) !!}
                                    </div>
                                </div>
                            <div class="row">
                                <div class="col-md-5 form-group">
                                    <select name="category" class="form-control">
                                        @foreach($page_categories as $page_category)
                                            <option value="{{$page_category->id}}">{{$page_category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                                <div class="row">
                                    <div class="col-md-2 form-group">
                                        {!! Form::submit('Добавить', array('class' => 'form-control waves')) !!}
                                    </div>
                                </div>

                                {!! Form::close() !!}


                        </div>
                    </div>
                </div>

            </div> <!-- container -->

        </div> <!-- content -->

        <footer class="footer">
            © 2017. All rights reserved.
        </footer>

    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->





@endsection
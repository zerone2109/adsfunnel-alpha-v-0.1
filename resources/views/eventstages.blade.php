@extends('layouts.app')

@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="panel panel-default">
                            <div class="panel-heading">Тригеры на этапы воронки</div>
                            <div class="panel-body">
                                <select class="form-control select2 project_select_stages"
                                        data-placeholder="Выберите проект">
                                    <option value="" disabled selected></option>
                                    @foreach ($projects as $project)
                                        <option value="{{ $project->ID }}">{{ $project->Project_name }}</option>
                                    @endforeach
                                </select>
                                <div class="triger-block m-t-20"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@extends('layouts.app')

@section('content')
    {{--<div class="container">
        <table class="table">
            @foreach($sites as $site)
                <tr>
                    <td><a href="{{ URL::to('projects/'.$site->ID) }}">{{ $site->Site_name }}</a></td>
                </tr>
            @endforeach
        </table>
    </div>--}}

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <h4 class="m-t-0 header-title"><b>Список сайтов</b></h4><br>

                            <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                </tr>
                                </thead>


                                <tbody>
                                @foreach($sites as $site)
                                    <tr>
                                        <td><a href="{{ URL::to('projects/'.$site->ID) }}">{{ $site->Site_name }}</a></td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div> <!-- container -->

        </div> <!-- content -->

        <footer class="footer">
            © 2017. All rights reserved.
        </footer>

    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->

@endsection





<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Acquisition extends Model
{
    //
    protected $table = 'Acquisition';
    protected $primaryKey = 'ID';
    public function index()
    {
        /*$users = DB::table($this->table)->get();
        return $users;*/
    }

    // relations
    public function client(){
        return $this->belongsTo('App\Client','client_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_stages extends Model
{
    //

    protected $table = 'user_stages';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = ['stage_id','project_id'];

//    public function event()
//    {
//        return $this->belongsTo('App\Event','event_id');
//    }
    public function stage()
    {
        return $this->belongsTo('App\Stage','stage_id');
    }
    public function session()
    {
        return $this->belongsTo('App\Session','session_id');
    }

}

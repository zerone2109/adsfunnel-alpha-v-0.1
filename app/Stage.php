<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stage extends Model
{

    protected $table = 'Stages';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function project()
    {
        return $this->belongsTo('App\Project','project_id');
    }
}

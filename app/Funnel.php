<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Funnel extends Model
{
    //

    protected $table = 'Funnels';
    protected $primaryKey = 'id';


    public function client()
    {
        return $this->belongsTo('App\Client','client_id');
    }
    public function project()
    {
        return $this->belongsTo('App\Project','project_id');
    }
    public function category()
    {
        return $this->belongsTo('App\Page_category','category_id');
    }
    public function stages()
    {
        return $this->hasMany('App\User_stages','funnel_id');
    }

}

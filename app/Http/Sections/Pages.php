<?php

namespace App\Http\Sections;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminDisplayFilter;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

/**
 * Class Pages
 *
 * @property \App\Page $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Pages extends Section
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Редактор страниц';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {

        $table = AdminDisplay::table()->setFilters(
            AdminDisplayFilter::field('site_id')->setTitle('site ID [:value]')
        )->setApply(function($query) {
            $query->orderBy('ID', 'desc');
        })->setColumns([
            AdminColumn::link('Page_url')->setLabel('Урл')->setWidth('400px'),
            AdminColumn::link('Page_type')->setLabel('Тип страницы')->setWidth('400px'),
            AdminColumn::link('page_score')->setLabel('Вес страницы')->setWidth('400px'),
            AdminColumn::link('page_intent')->setLabel('Тег страницы')->setWidth('400px'),
        ])->paginate(10);
        return  $table;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $form = AdminForm::form()->setElements([
            AdminFormElement::text('Page_url', 'Page_url')->required(),
            AdminFormElement::select( 'page_type', null, array('vizit' => 'Визит','product' => 'Продукт','ordering' => 'Оформление заказа'))->setLabel('Тип страницы')
                ->setHtmlAttribute('placeholder', 'Выберите страну')
                ->required(),
            AdminFormElement::select('Site_id')->setLabel('Сайт')
                ->setModelForOptions(\App\Site::class)
                ->setHtmlAttribute('placeholder', 'Выберите сайт')
                ->setDisplay('Site_name')
                ->required(),
            AdminFormElement::text('page_score', 'page_score')->required(),
            AdminFormElement::text('page_intent', 'page_intent'),
        ]);
        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}

<?php

namespace App\Http\Controllers;

use App\Client;
use App\Client_pageviews;
use App\Client_Site;
use App\Contact;
use App\Device;
use App\Page;
use App\Page_category;
use App\Page_type;
use App\Project;
use App\Session;
use App\Site;
use App\Http\Controllers\Controller;
use Request;
use Illuminate\Support\Facades\DB;
use View;

class ProjectController extends Controller
{
    //


    public function index(){
        $data['sites'] = Site::all();
        return View::make("projects")
            ->with($data)
            ->render();
    }


    // single project
    public function show_project_list($id){
        $data['sites'] = Site::all();
        $site = Site::find($id);
        $data['site'] = $site;

        $data['pages'] = Page::where([
                ['Site_id', '=', $id],
        ])->get();
        $data['page_categories'] = Page_category::where('site_id','=',$id)->get();
        $data['page_types'] = Page_type::all();

        return View::make("project")
            ->with($data)
            ->render();

    }

    // single project page
    public function project_single_page($id,$page){
        $data['sites'] = Site::all();
        $data['site'] = Site::find($id);
        $data['page'] = Page::find($page);
        $data['page_categories'] = Page_category::where('site_id','=',$id)->get();
        $data['page_types'] = Page_type::all();
        return View::make("pages")
            ->with($data)
            ->render();

    }

    public function add_page($id){

        $data['sites'] = Site::all();
        $data['site'] = Site::find($id);
        $data['page_categories'] = Page_category::where('site_id','=',$id)->get();
        $data['page_types'] = Page_type::all();
        return View::make("add_page")
            ->with($data)
            ->render();
    }
    public function create_page(){
        $this->inputs = Request::all();

        if(Request::has('add_new')){
            $page = new Page;
            $page->Site_id = $this->inputs['site'];
            $page->Page_url = $this->inputs['url_value'];
            $page->page_type_id = $this->inputs['page_type'];
            $page->page_score = $this->inputs['score'];
            $page->category_id = $this->inputs['category'];
            $page->save();

            return redirect('/projects/'.$this->inputs['site']);
        }
    }

    public function update_page(){
        $this->inputs = Request::all();
        if(Request::has('score')) {
            $this->update_single_page($this->inputs['update_value'], $this->inputs['page'], $this->inputs['page_type'],$this->inputs['score'],$this->inputs['category'],$this->inputs['title'],$this->inputs['price']);
        }
        else {
            switch ($this->inputs['condition']) {
                case '=':
                    $this->update_where($this->inputs['update_value'], $this->inputs['site'], $this->inputs['page_type']);
                    break;
                case 'like':
                    $this->update_like($this->inputs['update_value'], $this->inputs['site'], $this->inputs['page_type']);
                    break;
            }
        }

        return redirect('/projects/'.$this->inputs['site']);
    }

    public function update_like($reg_val,$site_id,$page_type){
        \App\Page::where('Page_url','LIKE', '%'.$reg_val.'%')
            ->where('Site_id', $site_id)
            ->update(['page_type_id' => $page_type]);

    }
    public function update_where($reg_val,$site_id,$page_type){
        \App\Page::where('Page_url',$reg_val)
            ->where('Site_id', $site_id)
            ->update(['page_type_id' => $page_type]);


    }



    public function update_category_page(){
        $this->inputs = Request::all();
        if(Request::has('score')) {
            $this->update_single_page($this->inputs['update_value'], $this->inputs['page'], $this->inputs['page_type'],$this->inputs['score'],$this->inputs['category']);
        }
        else {
            switch ($this->inputs['condition']) {
                case '=':
                    $this->update_category_where($this->inputs['update_value'], $this->inputs['site'], $this->inputs['category']);
                    break;
                case 'like':
                    $this->update_category_like($this->inputs['update_value'], $this->inputs['site'], $this->inputs['category']);
                    break;
            }
        }

        return redirect('/projects/'.$this->inputs['site']);
    }

    public function update_category_like($reg_val,$site_id,$page_type){
        \App\Page::where('Page_url','LIKE', '%'.$reg_val.'%')
            ->where('Site_id', $site_id)
            ->update(['category_id' => $page_type]);

    }
    public function update_category_where($reg_val,$site_id,$page_type){
        \App\Page::where('Page_url',$reg_val)
            ->where('Site_id', $site_id)
            ->update(['category_id' => $page_type]);

    }
    public function update_single_page($url,$id,$page_type,$score,$category,$title,$price){
        \App\Page::where('ID',$id)
            ->update(['Page_url' => $url ,'page_type_id' => $page_type,'page_score'=> $score, 'category_id' => $category, 'price' => $price, 'title' => $title]);


    }


    public function category_change(){
        $this->inputs = Request::all();
        $site = $this->inputs['site'];
        $categories = explode(',',$this->inputs['categories']);
        $current_categories = Page_category::where('site_id','=',$site)->get();


        foreach ($categories as $category) {
            Page_category::updateOrCreate(["name" => $category,"site_id" => $site ],["name" => $category ]);//записываем в базу
        }

        return $categories;
    }
    public function category_delete(){
        $this->inputs = Request::all();
        $site = $this->inputs['site'];
        $category = $this->inputs['category'];
        Page_category::where([
            ["name", '=', $category],
            ["site_id", '=',$site ]
        ])->delete();

        return $category;
    }
}

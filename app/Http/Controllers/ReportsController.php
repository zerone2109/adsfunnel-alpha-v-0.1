<?php

namespace App\Http\Controllers;

use App\Client;
use App\Client_pageviews;
use App\Client_Site;
use App\Contact;
use App\Device;
use App\Funnel;
use App\Page;
use App\Page_category;
use App\Page_type;
use App\Project;
use App\Session;
use App\User_stages;
use App\Site;
use App\Http\Controllers\Controller;
use Request;
use Illuminate\Support\Facades\DB;
use View;
use Carbon\Carbon;

class ReportsController extends Controller
{
    public $acquisitions_data;
    public function index(){

    }

    public function kpi_json(){
        $this->acquisitions_data = array();
        $funnels = Funnel::all();
        foreach ($funnels as $funnel){
            foreach($funnel->stages as $funnel_stage){

                if($funnel_stage->stage_id == 5){
                    //json
                    $chanel = $funnel_stage->session->page_views->first()->acquisition->Campaign_medium;
                    $date = $funnel_stage->created_at;
                    $funnel_id = $funnel->id;
                    $project_id = $funnel->project_id;
                    $category_id = $funnel->category_id;
                    $price = $funnel_stage->session->user_events->where('event_id','=',15)->where('Created_at','<=',$date)->last()->page_views->page->price;

                    array_push($this->acquisitions_data,[
                      'chanel'=>  $chanel,
                      'date'=>  $date,
                      'funnel_id'=>  $funnel_id,
                      'project_id'=>  $project_id,
                      'price'=>  $price,
                      'category_id'=>  $category_id,
                    ]);

                }
            }

        }

        return json_encode($this->acquisitions_data);

    }

    public function session_chanel_sum_json(){
        $this->acquisitions_data = array();
        $sessions = Session::all();
        foreach ($sessions as $session){

                array_push($this->acquisitions_data,[
                    'session_id'=>  $session->ID,
                    'session_duration'=>  strtotime("1970-01-01 $session->Session_duration UTC"),
                    'session_value'=>  $session->Value_of_the_session,
                    'campaign_medium'=>  $session->page_views->first()->acquisition->Campaign_medium,
                ]);
        }
        return json_encode($this->acquisitions_data);

    }

}

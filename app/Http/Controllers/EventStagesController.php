<?php

namespace App\Http\Controllers;


use App\Client;
use App\Client_pageviews;
use App\Client_Site;
use App\Contact;
use App\Condition;
use App\Device;
use App\Event;
use App\Page;
use App\Page_category;
use App\Project;
use App\User_stages;
use App\Stage_condition;
use App\Stage;
use App\Session;
use App\Site;
use App\Webhook;
use App\Http\Controllers\Controller;
use Request;
use Illuminate\Support\Facades\DB;
use View;

class EventStagesController extends Controller
{
    public function index(){
        $data['projects'] = Project::all();
        return View::make("eventstages")
            ->with($data)
            ->render();
    }

    public function conditions_stages_get(){
        $this->inputs = Request::all();
        $data['events'] = Event::all();
//        $data['webhooks'] = Webhook::all()->where('project_id',$this->inputs['project_id']);
        $data['stages'] = Stage::all();

//        $data['conditions'] = Project::find($this->inputs['project_id'])->webhooks;
        $data['conditions'] = Stage_condition::all()->where('project_id',$this->inputs['project_id'])->groupBy('stage_id');
//        dd($data['conditions']);
        return View::make("partials.conditions_stage")
            ->with($data)
            ->render();
    }

    public function condition_stage_save(){
        $this->inputs = Request::all();
        $stage_id = $this->inputs['stage_id'];
        $project_id = $this->inputs['project_id'];
        foreach($this->inputs['events_id'] as $event_id){
            Stage_condition::updateOrCreate(["event_id" => $event_id,"project_id" => $project_id, "stage_id" => $stage_id ],["project_id" => $project_id ]);//записываем в базу
        }

        return $this->inputs;
    }

    public function conditions_stage_add(){
        $this->inputs = Request::all();

        foreach($this->inputs['events_id'] as $event_id){
            $stage_condition = new Stage_condition;
            $stage_condition->event_id = $event_id;
            $stage_condition->project_id = $this->inputs['project_id'];
            $stage_condition->stage_id = $this->inputs['stage_id'];
            $stage_condition->save();
        }

        return $this->inputs;
    }

    public function condition_stage_delete(){
        $this->inputs = Request::all();
        $stage_id = $this->inputs['stage_id'];
        $event_id = $this->inputs['event_id'];
        $project_id = $this->inputs['project_id'];
        Stage_condition::where([
            ["stage_id", '=', $stage_id],
            ["event_id", '=',$event_id ],
            ["project_id", '=',$project_id ]
        ])->delete();

        return $this->inputs;
    }

//    public function webhook_delete(){
//        $this->inputs = Request::all();
//        $webhook_id = $this->inputs['webhook_id'];
//        Condition::where("webhook_id", "=",$webhook_id)->delete();
//        Webhook::where("id","=", $webhook_id)->delete();
//
//        return $this->inputs;
//    }
}

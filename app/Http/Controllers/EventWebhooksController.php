<?php

namespace App\Http\Controllers;

use App\Client;
use App\Client_pageviews;
use App\Client_Site;
use App\Contact;
use App\Condition;
use App\Device;
use App\Event;
use App\Page;
use App\Page_category;
use App\Project;
use App\Session;
use App\Site;
use App\Webhook;
use App\Http\Controllers\Controller;
use Request;
use Illuminate\Support\Facades\DB;
use View;

class EventWebhooksController extends Controller
{
    public function index(){
        $data['projects'] = Project::all();
        return View::make("eventwebhooks")
            ->with($data)
            ->render();
    }

    public function conditions_get(){
        $this->inputs = Request::all();
        $data['events'] = Event::all();
        $data['webhooks'] = Webhook::all()->where('project_id',$this->inputs['project_id']);
//        $data['conditions'] = Project::find($this->inputs['project_id'])->webhooks;
        $data['conditions'] = Condition::all()->where('project_id',$this->inputs['project_id'])->groupBy('webhook_id');
//        dd($data['conditions']);
        return View::make("partials.conditions")
            ->with($data)
            ->render();
    }

    public function condition_save(){
        $this->inputs = Request::all();
        $webhook_id = $this->inputs['webhook_id'];
        $project_id = $this->inputs['project_id'];

        foreach($this->inputs['events_id'] as $event_id){
              Condition::updateOrCreate(["event_id" => $event_id,"project_id" => $project_id, "webhook_id" => $webhook_id ],["project_id" => $project_id ]);//записываем в базу
        }

        return $this->inputs;
    }

    public function webhook_add(){
        $this->inputs = Request::all();


        $webhook = new Webhook;
        $webhook->name = $this->inputs['name'];
        $webhook->url = $this->inputs['link'];
        $webhook->description = $this->inputs['description'];
        $webhook->project_id = $this->inputs['project_id'];
        $webhook->save();

        foreach($this->inputs['events_id'] as $event_id){
            $condition = new Condition;
            $condition->event_id = $event_id;
            $condition->project_id = $this->inputs['project_id'];
            $condition->webhook_id = $webhook->id;
            $condition->save();
        }

        return $this->inputs;
    }

    public function condition_delete(){
        $this->inputs = Request::all();
        $webhook_id = $this->inputs['webhook_id'];
        $event_id = $this->inputs['event_id'];
        $project_id = $this->inputs['project_id'];
        Condition::where([
            ["webhook_id", '=', $webhook_id],
            ["event_id", '=',$event_id ],
            ["project_id", '=',$project_id ]
        ])->delete();

        return $this->inputs;
    }

    public function webhook_delete(){
        $this->inputs = Request::all();
        $webhook_id = $this->inputs['webhook_id'];
        Condition::where("webhook_id", "=",$webhook_id)->delete();
        Webhook::where("id","=", $webhook_id)->delete();

        return $this->inputs;
    }
}



<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    //
    protected $table = 'Site';
    public $timestamps = false;
    protected $primaryKey = 'ID';

    public function index()
    {
        $Sites = DB::table($this->table)->get();
        return $Sites;
    }

    //что разрешаем править
    protected $fillable = [
        'Site_name', 'project_id',
    ];


    // relations

    public function pages()
    {
        return $this->hasMany('App\Page','Page_id');
    }

    public function page_categories()
    {
        return $this->hasMany('App\Page_category','site_id');
    }
    public function project()
    {
        return $this->belongsTo('App\Project','project_id');
    }

    public function client_site()
    {
        return $this->belongsTo('App\Client_Site');
    }






}

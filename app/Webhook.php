<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Webhook extends Model
{
    //

    protected $table = 'webhooks';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = ['project_id','name'];


    public function project()
    {
        return $this->belongsTo('App\Project','project_id');
    }
}

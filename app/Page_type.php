<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page_type extends Model
{
    //
    protected $table = 'pages_type';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = ['name', 'value'];



}

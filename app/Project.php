<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    //
    protected $table = 'Project';
    protected $primaryKey = 'ID';

    public function index()
    {
        $Projects = DB::table($this->table)->get();
        return $Projects;
    }

    // relations


    public function pages()
    {
        return $this->hasMany('App\Site');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    //

    protected $table = 'Devices';
    protected $primaryKey = 'ID';

    public function index()
    {
        $Devices = DB::table($this->table)->get();
        return $Devices;
    }

    // relations
    public function session()
    {
        return $this->belongsTo('App\Session');
    }
}

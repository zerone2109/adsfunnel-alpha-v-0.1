<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //
    protected $table = 'tags';
    public function index(){

    }
    // relations
    public function client(){
        return $this->belongsTo('App\Client','client_id');
    }

}


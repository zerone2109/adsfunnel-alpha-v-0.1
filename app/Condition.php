<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Condition extends Model
{
    //

    protected $table = 'conditions_of_webhooks';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = ['project_id','event_id','webhook_id'];

    public function project()
    {
        return $this->belongsTo('App\Project','project_id');
    }

    public function event()
    {
        return $this->belongsTo('App\Event','event_id');
    }

    public function webhook()
    {
        return $this->belongsTo('App\Webhook','webhook_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_event extends Model
{
    //

    protected $table = 'User events';
    protected $primaryKey = 'ID';

    public function event()
    {
        return $this->belongsTo('App\Event','event_id');
    }
    public function page_views()
    {
        return $this->belongsTo('App\Client_pageviews','Pageviews_id');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database|

class Client extends Model
{
    //
    protected $table = 'Client';
    protected $primaryKey = 'ID';

    public function index()
    {
        /*$users = DB::table($this->table)->get();
        return $users;*/
    }


    // relations
    public function client_site()
    {
        return $this->hasOne('App\Client_Site','Client_id');
    }
    public function contact()
    {
        return $this->belongsTo('App\Contact','Contact_id');
    }



    public function tags()
    {
        return $this->hasMany('App\Tag','client_id');
    }


}

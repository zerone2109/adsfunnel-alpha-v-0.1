<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page_category extends Model
{
    //

    protected $table = 'pages_category';
    public $timestamps = false;
    protected $fillable = ['name', 'site_id'];


    public function site()
    {
        return $this->belongsTo('App\Site','site_id');
    }

}

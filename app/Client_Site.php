<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client_Site extends Model
{
    //
    protected $table = 'Client-Site';
    protected $primaryKey = 'ID';

    public function index()
    {
        $Client_Project = DB::table($this->table)->get();
        return $Client_Project;
    }


    // relations
    public function client()
    {
        return $this->belongsTo('App\Client','Client_id');
    }

    public function site()
    {
        return $this->hasOne('App\Site','Site_id');
    }
    public function session()
    {
        return $this->hasMany('App\Session','Client-Site_id');
    }

    public function client_pageviews()
    {
        return $this->hasMany('App\Client_pageviews','	Client-Site_id');
    }
}

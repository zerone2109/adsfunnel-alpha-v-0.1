<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    //
    protected $table = 'Session';
    public $timestamps = false;
    protected $primaryKey = 'ID';

    public function index()
    {
        $Sessions = DB::table($this->table)->get();
        return $Sessions;
    }


    // relations
    public function client_site()
    {
        return $this->belongsTo('App\Client_Site');
    }

    public function page_views()
    {
        return $this->hasMany('App\Client_pageviews','Session_id');
    }

    public function user_events()
    {
        return $this->hasMany('App\User_event','Session_id');
    }
    public function device()
    {
        return $this->hasOne('App\Device');
    }





}

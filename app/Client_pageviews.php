<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client_pageviews extends Model
{
    //

    protected $table = 'Client pageviews';
    protected $primaryKey = 'ID';

    public function index()
    {
        $Client_pageviews = DB::table($this->table)->get();
        return $Client_pageviews;
    }


    // relations
    public function page()
    {
        return $this->belongsTo('App\Page','Page_id');
    }

    public function session()
    {
        return $this->belongsTo('App\Session','Session_id');
    }
    public function acquisition()
    {
        return $this->belongsTo('App\Acquisition','acquisition_id');
    }




}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    //
    protected $table = 'Pages';
    public $timestamps = false;
    protected $primaryKey = 'ID';

    public function index()
    {
        $Pages = DB::table($this->table)->get();
        return $Pages;
    }


    // relations

    public function site()
    {
        return $this->belongsTo('App\Site','Site_id');
    }
    public function type()
    {
        return $this->belongsTo('App\Page_type','page_type_id');
    }

    public function page_views()
    {
        return $this->hasMany('App\Client_pageviews','Page_id');
    }
    public function category()
    {
        return $this->belongsTo('App\Page_category','category_id');
    }



}

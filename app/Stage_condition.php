<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stage_condition extends Model
{
    //

    protected $table = 'conditions_of_stages';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = ['stage_id','project_id','event_id'];

    public function project()
    {
        return $this->belongsTo('App\Project','project_id');
    }

    public function event()
    {
        return $this->belongsTo('App\Event','event_id');
    }

    public function stage()
    {
        return $this->belongsTo('App\Stage','stage_id');
    }
}

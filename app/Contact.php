<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    //

    protected $table = 'Contact';
    protected $primaryKey = 'ID';

    public function index()
    {
        $Contacts = DB::table($this->table)->get();
        return $Contacts;
    }


    // relations

    public function clients()
    {
        return $this->hasMany('App\Client','Contact_id');
    }
}

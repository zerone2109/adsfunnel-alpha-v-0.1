<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //

    protected $table = 'events';
    protected $primaryKey = 'id';


    public function user_events()
    {
        return $this->hasMany('App\User_event','event_id');
    }
}

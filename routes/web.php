<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/projects');
});

Auth::routes();

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});

Route::get('/report-kpi-json', 'ReportsController@kpi_json');
Route::get('/session-chanel-sum-json', 'ReportsController@session_chanel_sum_json');

// Откл регистрацию.
Route::get('/register', function () {
    return redirect('/projects');
});




